//#define TIMING
//#define DEBUG

#include <iostream>
#include <vector>
#include <bitset>
#include <fstream>
#include <limits>
#include <cmath>
#include <algorithm>
#include <forward_list>
#include <list>

#ifdef TIMING
#include <chrono>
using namespace std::chrono;
#endif

using namespace std;

const int PERM_LIMIT = 10;

struct ansModelWrongs
{
    uint64_t answer;
    uint64_t wrongs;
};

struct answerWrong
{
    vector<uint64_t> answers;
    int wrongs{};
};

uint64_t oneCounter (uint64_t a) {
    return __builtin_popcountll(a);
}


void printBin (uint64_t num, int len, ostream &out) {
    uint64_t mask = 1ull << (len - 1);
    for (int i = 0; i < len; ++i) {
        if (mask & num)
            out << "1";
        else
            out << "0";

        num = num << 1ull;
    }
}

bool checkCode (uint64_t ansModel, int numStuds, const vector<answerWrong> &answers) {
    bool correct = true;
    uint64_t i = 0;
    while (correct && i < numStuds) {
        correct = (oneCounter(answers[i].answers[0] ^ ansModel) == (answers[i].wrongs));
        i++;
    }
    return correct;
}

bool checkCodeRange (uint64_t ansModel, int numStuds, const vector<answerWrong> &answers,
                     ansModelWrongs &ansModAndWrongs, int half) {
    bool correct = true;
    int i = 0;
    uint64_t faults = 0;

    while (correct && i < numStuds) {
        uint64_t res = oneCounter(answers[i].answers[half] ^ ansModel);
        correct = res <= answers[i].wrongs;
        faults |= res << i * 5;
        i++;
    }
    ansModAndWrongs.wrongs = faults;
    ansModAndWrongs.answer = ansModel;
    return correct;
}

uint64_t newPermutation (uint64_t currPerm) {
    //Bron: https://graphics.stanford.edu/~seander/bithacks.html#NextBitPermutation
    uint64_t t = currPerm | (currPerm - 1ull);
    // t gets currPerm's least significant 0 bits set to 1
    // Next set to 1 the most significant bit to change,
    // set to 0 the least significant ones, and add the necessary 1 bits.
    // A complete explanation written by us can be found in the project report.
    return ((t + 1ull) | (((~t & (t + 1ull)) - 1ull) >> (__builtin_ctzll(currPerm) + 1ull)));
}

void flapper (uint64_t ans, int wrongs, int numAns, int numStuds, forward_list<uint64_t> &ansModels,
              const vector<answerWrong> &answers) {
    uint64_t W = (1ull << wrongs) - 1ull;
    uint64_t V = newPermutation(W << (numAns - wrongs));

    do {
        uint64_t A = ans ^W;

        if (checkCode(A, numStuds, answers))
            ansModels.push_front(A);

        W = newPermutation(W);
    } while (W != V);

}

void flap_per (int numAns, int numStuds, const vector<answerWrong> &answers, int half,
               vector<ansModelWrongs> &ansModAndWrongses) {

    auto limit = (1ull << (numAns / 2 + ((half ^ 1) & (numAns & 1))));
    for (uint64_t i = 0; i < limit; ++i) {
        ansModelWrongs ansModAndWrongs{};
        if (checkCodeRange(i, numStuds, answers, ansModAndWrongs, half))
            ansModAndWrongses.push_back(ansModAndWrongs);
    }
}

int checkWrongs (const ansModelWrongs &ans1, const ansModelWrongs &ans2, uint64_t wrongs) {
    uint64_t res = ans1.wrongs + ans2.wrongs;
    if (wrongs < res) {
        return 1;
    } else if (res < wrongs) {
        return -1;
    } else {
        return 0;
    }
}

uint64_t combine (vector<ansModelWrongs> &ansModels1, vector<ansModelWrongs> &ansModels2,
                  const uint64_t wrongs, int numAns) {

    uint64_t count = 0;
    uint64_t singleAns = 0;

    if (!(ansModels1.empty() || ansModels2.empty())) {
        auto comp = [] (const ansModelWrongs &a, const ansModelWrongs &b) {
            return a.wrongs < b.wrongs;
        };

        sort(ansModels1.begin(), ansModels1.end(), comp);
        sort(ansModels2.begin(), ansModels2.end(), comp);

        size_t len1 = ansModels1.size();
        size_t len2 = ansModels2.size();
        vector<size_t> found(len1, len2);

        list<pair<size_t, size_t>> stack = {make_pair(0, len2 - 1)};

        while (!stack.empty()) {
            auto elem = stack.back();
            stack.pop_back();

            if (found[elem.first] <= elem.second)
                continue;

            found[elem.first] = elem.second;


            int leg = checkWrongs(ansModels1[elem.first], ansModels2[elem.second], wrongs);
            if (leg == 0) {

                if (count == 0) {
                    singleAns = ansModels1[elem.first].answer << (numAns / 2) | ansModels2[elem.second].answer;
                }

                size_t l = 0;
                while (elem.first + l < len1 && ansModels1[elem.first].wrongs == ansModels1[elem.first + l].wrongs)
                    l++;

                size_t r = 0;
                while (elem.second - r < len2 && ansModels2[elem.second].wrongs == ansModels2[elem.second - r].wrongs)
                    r++;

                count += l * r;

                if (elem.first < len1 - 1)
                    stack.emplace_front(elem.first + l, elem.second);

                if (elem.second > 0)
                    stack.emplace_front(elem.first, elem.second - r);

            } else if (leg > 0) {
                size_t r = 0;
                while (elem.second - r < len2 && ansModels2[elem.second].wrongs == ansModels2[elem.second - r].wrongs)
                    r++;

                if (elem.second > 0)
                    stack.emplace_front(elem.first, elem.second - r);

            } else {
                size_t l = 0;
                while (elem.first + l < len1 && ansModels1[elem.first].wrongs == ansModels1[elem.first + l].wrongs)
                    l++;

                if (elem.first < len1 - 1)
                    stack.emplace_front(elem.first + 1, elem.second);
            }
        }
    }

    if (count == 0 || count > 1) {
        return count;
    } else {
        return singleAns | 1ull << 63;
    }
}

#ifdef DEBUG
void printVector (const vector<answerWrong> &answers, int length, int numAns) {
    for (const answerWrong &ans : answers) {
        for (int i = 0; i < ans.answers.size(); ++i) {
            if (numAns & 1 && i == 0 && ans.answers.size() == 2) {
                printBin(ans.answers[i], static_cast<int>(length / ans.answers.size()) + 1, cerr);
            } else {
                printBin(ans.answers[i], static_cast<int>(length / ans.answers.size()), cerr);
            }
            cerr << "\t";
        }
        cerr << "\t" << ans.wrongs << endl;
    }
    cerr << flush;
}

#endif

#ifdef TIMING
void logInfo (high_resolution_clock::time_point t1, high_resolution_clock::time_point t2) {
    auto duration = duration_cast<milliseconds>(t2 - t1).count();
    cerr << "In: " << duration << endl << flush;
}

#endif

int readData (std::istream &in, vector<answerWrong> &answers) {
    int numStuds;
    int numAns;
    in >> numStuds;
    in >> numAns;

    string tS;
    uint64_t ansStud;
    int amountGood;
    for (int i = 0; i < numStuds; ++i) {
        answerWrong ansWrong;
        vector<uint64_t> answer;

        in >> tS;
        in >> amountGood;
        ansStud = stoull(tS, nullptr, 2);
        if (amountGood <= numAns / 2) {
            ansStud = ansStud ^ ((1ull << numAns) - 1);
            amountGood = numAns - amountGood;
        }
        if (numAns >= PERM_LIMIT) {
            answer.push_back(ansStud >> (numAns / 2));
            answer.push_back(ansStud & (1ull << numAns / 2) - 1);
        } else {
            answer.push_back(ansStud);
        }

        ansWrong.answers = answer;

        int amountWrong = numAns - amountGood;
        ansWrong.wrongs = amountWrong;

        answers.push_back(ansWrong);
    }

    return numAns;
}

int main (int argc, char **argv) {
    vector<answerWrong> answers;
    uint64_t wrongs = 0;

    int numAns = 0;

    if (argc > 1) {
        ifstream in(argv[1]);
        numAns = readData(in, answers);
    } else {
        numAns = readData(cin, answers);
    }

    sort(answers.begin(), answers.end(),
         [] (const answerWrong &a, const answerWrong &b) { return a.wrongs < b.wrongs; });

    for (int i = 0; i < answers.size(); ++i) {
        wrongs |= static_cast<uint64_t>(answers[i].wrongs) << i * 5;
    }

    auto numStuds = static_cast<int>(answers.size());

    if (numAns < PERM_LIMIT){
#ifdef DEBUG
        cerr << "PERMS" << endl;
        printVector(answers, numAns, numAns);
#endif
        answerWrong best = answers.front();
        answers.erase(answers.begin());
        numStuds--;
        forward_list<uint64_t> ansModels;


#ifdef TIMING
        high_resolution_clock::time_point t1 = high_resolution_clock::now();
#endif

        flapper(best.answers[0], best.wrongs, numAns, numStuds, ansModels, answers);

#ifdef TIMING
        high_resolution_clock::time_point t2 = high_resolution_clock::now();

        logInfo(t1, t2);
#endif

        int len = static_cast<int>(distance(ansModels.begin(), ansModels.end()));
        if (len == 1) {
            printBin(ansModels.front(), numAns, cout);
            cout << endl;
        } else {
            cout << len << " solutions" << endl;
        }
    } else {
#ifdef DEBUG
        cerr << "COMBS" << endl;
        printVector(answers, numAns, numAns);
#endif

#ifdef TIMING
        high_resolution_clock::time_point t1 = high_resolution_clock::now();
#endif

        vector<ansModelWrongs> ansModAndWrongses1;
        ansModAndWrongses1.reserve(1ull << (numAns / 2 + 1));

        flap_per(numAns, numStuds, answers, 0, ansModAndWrongses1);

        vector<ansModelWrongs> ansModAndWrongses2;
        ansModAndWrongses2.reserve(1ull << (numAns / 2));

        flap_per(numAns, numStuds, answers, 1, ansModAndWrongses2);

        uint64_t res = combine(ansModAndWrongses1, ansModAndWrongses2, wrongs, numAns);

#ifdef TIMING
        high_resolution_clock::time_point t2 = high_resolution_clock::now();

        logInfo(t1, t2);
#endif

        if (res & 1ull << 63) {
            printBin(res, numAns, cout);
            cout << endl;
        } else {
            cout << res << " solutions" << endl;
        }
    }
    return 0;
}